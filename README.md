# silhouette_island

Archlinux i3 & conky config

## Preview

## clean
![light](/preview/Clean.png)
<br />

## Eww
![light](/preview/Eww.png)
<br />

## Neofetch
![light](/preview/Neofetch.png)
<br />

## Fonts for polybar

- **Shakerato-PERSONAL**
- **Material\-Design\-Iconic\-Font**
- **Font Awesome 5 Free**
- **feather**

## Fonts for conky
- **Signerica Fat**
- **Kiye Sans**
- **Glacial Indifference**

## Details
- **Distro** ArchLinux ;)
- **WM** i3-gaps
- **Panel** Polybar
- **Weather Widgets and music player** Conky
- **Program Launcher** rofi


## You Need
- **Polybar**
- **Conky-lua-nv**
- **jq**
- **curl**
- **Mpd, Mpc, Ncmpcpp**
- **Eww**
- **Neofetch**
- **i3-gaps**
- **Wezterm**
